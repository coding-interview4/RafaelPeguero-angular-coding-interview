import { Component, OnInit } from '@angular/core';
import { QuestionsService } from '../../core/services/questions.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { QuestionModel } from '../../core/state/question.model';

@Component({
  selector: 'app-play',
  templateUrl: './play.component.html',
  styleUrls: ['./play.component.scss']
})
export class PlayComponent implements OnInit {
  questions$ = this.questionsService.questions$;
  gettingQuestions$ = this.questionsService.gettingQuestions$;
  correctAnswersCount: number = 0;
  incorrectAnswersCount: number = 0;
  getQuestionsSubscription: Subscription = this.route.queryParams
    .pipe(switchMap(params =>
      this.questionsService.getQuestions({
        type: params.type,
        amount: params.amount,
        difficulty: params.difficulty
      })
    )).subscribe({error: (error) => {
      alert("An error has occured Loading the questions")
    }});


  constructor(
    private readonly route: ActivatedRoute,
    private readonly questionsService: QuestionsService,
  ) { }

  ngOnInit(): void {
    this.checkSelectedAnswers();
  }

  onAnswerClicked(questionId: QuestionModel['_id'], answerSelected: string): void {
    this.questionsService.selectAnswer(questionId, answerSelected);
  }

  checkSelectedAnswers() {
    this.questions$.subscribe((questions : QuestionModel[]) => {
      this.correctAnswersCount = 0;
      this.incorrectAnswersCount = 0;
      questions.map(q=> {
        var correctAnswer = q.answers.find(a => a.isCorrect);
        if(q.selectedId == correctAnswer?._id) {
          this.correctAnswersCount += 1;
        } else {
          this.incorrectAnswersCount += 1;
        }
      } );
    });
  }

}
